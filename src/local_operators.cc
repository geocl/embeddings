#include "basics.h"
#include "utils.h"
#include "metric.h"
#include "predicates/predicates.h"
#include <iostream>  
#include <Eigen/Core>
#include <math.h>
#include "error.h"

using namespace std; 

// * Declaration of helper functions only visible in this source file * 

void get_receiving_vertices(Topology *topology, vector<int>& boundary_vertices, vector<int>& boundary_edges, vector<int>& receiving_vertices); 

int choose_receiving_vertex(Topology *topology, vector<int>& boundary_edges, vector<int>& receiving_vertices, MetricField *metricfield); 





// * Definition of local operators * 

unsigned short Topology::edge_split(edge_t edge, double proportion) {

    Vertices *pointset = this->pointset(); 
    int amb_dim = pointset->amb_dim(); 

    // Quit if edge not found
    if (!this->has_edge(edge) && !this->has_edge(rev(edge))) {
        cout << "(In Topology::edge_split()) Warning: edge " << edge[0] << ", " << edge[1] << " not found." << endl; 
        return 1; 
    }
    
    // Find the middle point of the edge
    double middle[amb_dim]; 
    double *p1 = pointset->get_vertex(edge[0]); 
    double *p2 = pointset->get_vertex(edge[1]); 

    for (int i = 0; i < amb_dim; i++) {
        middle[i] = (*(p1 + i)) * proportion + (*(p2 + i)) * (1 - proportion); 
    }

    // Insert the midpoint into pointset
    pointset->add_vertex(middle); 
    // Find the correct geometry attribute of the new point 
    short new_geom = pointset->infer_geometry(edge); 
    pointset->add_geometry(new_geom);

    // The index of the new vertex 
    int new_idx = pointset->nb() - 1; 

    // If `edge` identifies a triangle, then connect the third vertex of this triangle to the middle point 
    if (this->has_edge(edge)) {
        int third_vert = this->get_third_vertex(edge); 

        // Remove the previous triangle 
        this->delete_triangle(edge); 

        // Add the new split triangles 
        this->add_triangle(edge[0], third_vert, new_idx); 
        this->add_triangle(edge[1], third_vert, new_idx); 
    }
    
    // If `rev(edge)` identifies a triangle, then connect the third vertex 
    if (this->has_edge(rev(edge))) {
        int third_vert = this->get_third_vertex(rev(edge)); 

        // Remove the previous triangle 
        this->delete_triangle(rev(edge)); 

        // Add the new split triangles 
        this->add_triangle(edge[0], third_vert, new_idx); 
        this->add_triangle(edge[1], third_vert, new_idx); 
    }

    return 0; 
}



unsigned short Topology::edge_swap(edge_t edge) {
    Vertices *pointset = this->pointset(); 

    // Quit if the edge is not shared by 2 triangles (i.e. it is the boundary of the domain)
    if (!this->has_edge(edge) || !this->has_edge(rev(edge))) {
        cout << "(In Topology::edge_swap()) Warning: edge " << edge[0] << ", " << edge[1] << " is not shared by two triangles." << endl;  
        return 1; 
    }

    // Get the two third-vertices 
    int third_vert_1 = this->get_third_vertex(edge); 
    int third_vert_2 = this->get_third_vertex(rev(edge));

    // Pick one third-vertex as the receiving vertex (denoted `p` in Caplan 2020), then conduct "visibility test" using signed area/volume (See Caplan 2020 Equation (4)). Quit if the visibility test fails. 

    double *receiving_vert = pointset->get_vertex(third_vert_2);
    double *pa, *pb; 

    // Test the first edge that is non-adjacent to the receiving vertex
    pa = pointset->get_vertex(edge[1]);
    pb = pointset->get_vertex(third_vert_1); 
    if (orient2d(pa, pb, receiving_vert) < DET_THRESHOLD) {
        cout << "(In Topology::edge_swap()) Warning: edge " << edge[0] << ", " << edge[1] << " cannot be swapped because the visibility test is failed." << endl;  
        return 2; 
    }

    // Test the second edge that is non-adjacent to the receiving vertex 
    pa = pointset->get_vertex(third_vert_1); 
    pb = pointset->get_vertex(edge[0]); 
    if (orient2d(pa, pb, receiving_vert) < DET_THRESHOLD) {
        cout << "(In Topology::edge_swap()) Warning: edge " << edge[0] << ", " << edge[1] << " cannot be swapped because the visibility test is failed." << endl;  
        return 2; 
    }

    // Delete old triangles and insert new triangles 
    this->delete_triangle(edge); 
    this->delete_triangle(rev(edge)); 
    this->add_triangle(edge[0], third_vert_1, third_vert_2);
    this->add_triangle(edge[1], third_vert_1, third_vert_2);  

    return 0; 
}



unsigned short Topology::vertex_removal(int vert_idx, MetricField *metricfield) {
    Vertices *pointset = this->pointset(); 

    // Do nothing is current vertex is already removed
    if (!pointset->exists(vert_idx)) {
        // cout << "(In Topology::vertex_removal()) Warning: vertex " << vert_idx << " does not exist or is already removed." << endl; 
        return 1;
    }

    // Corner points cannot be removed 
    if (pointset->get_geometry(vert_idx) > 0) {
        cout << "(In Topology::vertex_removal()) Warning: removing the vertex " << vert_idx << " on the corner is not permitted." << endl; 
        return 2; 
    }    

    // Find all vertices that are connected to the vert_idx 
    vector<int> connected_vert; 
    this->get_v2v(vert_idx, connected_vert);  

    // Find the boundary edges of the "digged" polygon and remove all triangles adjacent to vert_idx 
    vector<int> boundary_edges;   // The i^th edge is stored at position 2i and 2i+1
    int bound_start_vert, bound_end_vert;    // A new edge, where vert_idx resides, needs to be created if vert_idx is a boundary vertex
    for (int i : connected_vert) {
        // Special treatment if {vert_idx, i} cannot query triangle -- vert_idx is a boundary vertex 
        if (!this->has_edge({vert_idx, i})) {
            bound_start_vert = i; 
            continue; 
        }

        // Special treatment if {i, vert_idx} cannot query triangle -- vert_idx is a boundary edge 
        if (!this->has_edge({i, vert_idx})) {
            bound_end_vert = i;
        }

        // Append the boundary edge 
        int third_vert = this->get_third_vertex({vert_idx, i}); 
        boundary_edges.push_back(i); 
        boundary_edges.push_back(third_vert); 
    }

    // Special treatment: add the new edge if vert_idx is a boundary vertex
    if (pointset->get_geometry(vert_idx) < 0) {
        // Append the new edge 
        boundary_edges.push_back(bound_start_vert); 
        boundary_edges.push_back(bound_end_vert);
    } 

    // Get all receiving vertices 
    vector<int> receiving_vert; 
    get_receiving_vertices(this, connected_vert, boundary_edges, receiving_vert); 
    
    // !! Bug about receiving vertices: throw a warning
    if (!receiving_vert.size()) {
        cout << "(In Topology::vertex_removal()) Warning: removing the vertex " << vert_idx << " is unsuccessful because no vertex in the digged polygon passed the visibility test." << endl; 
        return 3; 
    }

    // Choose a receiving vertex
    int recv_vert_idx = choose_receiving_vertex(this, boundary_edges, receiving_vert, metricfield); 

    // Remove all digged triangles 
    for (int i : connected_vert) {
        if (this->has_edge({vert_idx, i})) {
            this->delete_triangle({vert_idx, i}); 
        }
    }

    // Adding new triangles connecting boundaries and the receiving vertex
    for (int i = 0; i < boundary_edges.size() / 2; i++) {
        int edge_start_idx = boundary_edges[2 * i]; 
        int edge_end_idx = boundary_edges[2 * i + 1]; 

        // Skip the edges that are adjacent to the receiving vertex
        if (recv_vert_idx == edge_start_idx) {continue; }
        if (recv_vert_idx == edge_end_idx) {continue; }

        // Add triangle
        this->add_triangle(recv_vert_idx, edge_start_idx, edge_end_idx); 
    }

    // Mark vert_idx as removed 
    pointset->remove(vert_idx); 

    return 0; 
}



// Warning: vertex_smoothing only works for 2D case right now 
unsigned short Topology::vertex_smoothing(int vert_idx, MetricField *metricfield) {

    Vertices *pointset = this->pointset(); 

    // Do nothing is current vertex is already removed
    if (!pointset->exists(vert_idx)) {
        // cout << "(In Topology::vertex_smoothing()) Warning: vertex " << vert_idx << " does not exist or is already removed." << endl; 
        return 1;
    }

    // Corner points cannot be smoothed 
    if (pointset->get_geometry(vert_idx) > 0) {
        cout << "(In Topology::vertex_smoothing()) Warning: smoothing the vertex " << vert_idx << " on the corner is not permitted." << endl; 
        return 2; 
    } 

    // Find all vertices that are connected to the vert_idx 
    vector<int> connected_vert_along_entity; 
    this->get_v2v_along_entity(vert_idx, connected_vert_along_entity); 

    if (!connected_vert_along_entity.size()) {
        cout << "(In Topology::vertex_smoothing()) Warning: vertex " << vert_idx << " cannot be smoothed because there is no other vertex along the same geometric entity." << endl; 
        return 3; 
    }

    // * Compute the displacement for smoothing 
    // (See Eqn 3.10 in (Caplan 2019))

    Vector2d disp(0, 0); 
    double *coord_v = pointset->get_vertex(vert_idx); 
    Vector2d coord_v_vec(*coord_v, *(coord_v + 1)); 

    for (int i : connected_vert_along_entity) {
        double *coord_i = pointset->get_vertex(i);
        double len_edge = metricfield->get_distance(coord_v, coord_i); 
        Vector2d unit_vec(*(coord_i) - *(coord_v), 
                          *(coord_i + 1) - *(coord_v + 1)); 
        unit_vec = unit_vec / len_edge; 
        disp -= (1 - pow(len_edge, 4)) * exp(-pow(len_edge, 4)) * unit_vec;  
    }

    // Compute the coordinate of the smoothed vertex 
    Vector2d coord_v_new_vec = coord_v_vec + OMEGA * disp; 
    double *coord_v_new = coord_v_new_vec.data(); 

    vector<int> connected_vert; 
    this->get_v2v(vert_idx, connected_vert); 

    // Verify the new smoothed vertex passes visibility test for all the "interior" boundary edges 
    vector<int> boundary_edges; 
    for (int i : connected_vert) {
        // Special treatment
        if (!this->has_edge({vert_idx, i})) {
            continue; 
        }
        // Append the "interior" boundary edge
        int third_vert = this->get_third_vertex({vert_idx, i}); 
        boundary_edges.push_back(i); 
        boundary_edges.push_back(third_vert);
    }
    for (int i = 0; i < boundary_edges.size() / 2; i++) {
        int edge_start_idx = boundary_edges[2 * i]; 
        int edge_end_idx = boundary_edges[2 * i + 1]; 
        double *coord_start = pointset->get_vertex(edge_start_idx); 
        double *coord_end = pointset->get_vertex(edge_end_idx); 
        if (orient2d(coord_start, coord_end, coord_v_new) < DET_THRESHOLD) {
            cout << "(In Topology::vertex_smoothing()) Warning: vertex " << vert_idx << " cannot be smoothed because the new vertex is not visible to some polygon edges. Consider reducing the coefficient OMEGA." << endl;
            return 4; 
        }
    }

    // Remove the old vertex and add the new smoothed vertex
    short geom = pointset->get_geometry(vert_idx); 
    pointset->remove(vert_idx); 
    pointset->add_vertex(coord_v_new); 
    pointset->add_geometry(geom);    // The new vertex should have the same geometry attribute as before 
    int new_idx = pointset->nb() - 1; 

    // Remove the old triangles and add the new smoothed triangles
    for (int i : connected_vert) {
        if (this->has_edge({vert_idx, i})) {
            int third_vert = this->get_third_vertex({vert_idx, i}); 
            this->delete_triangle({vert_idx, i}); 
            this->add_triangle(new_idx, i, third_vert); 
        }
        // if (this->has_edge({i, vert_idx})) {
        //     int third_vert = this->get_third_vertex({i, vert_idx}); 
        //     this->delete_triangle({i, vert_idx}); 
        //     this->add_triangle(i, new_idx, third_vert); 
        // }
    }

    return 0; 
}





// * Definition of helper functions * 

// Given a the collection of boundary edges of a digged polygon, get the collection of all valid receiving vertices and store it at `receiving_vertices`
void get_receiving_vertices(Topology *topology, vector<int>& boundary_vertices, vector<int>& boundary_edges, vector<int>& receiving_vertices) {
    Vertices *pointset = topology->pointset(); 

    for (int vert_idx : boundary_vertices) { // iterate each vertex of the boundary
        // Indicates whether the current vertex is visible to all boundary edges. True by default
        bool visible_to_all_edges = true; 

        for (int i = 0; i < boundary_edges.size() / 2; i++) { // iterate each edge of the boundary 
            // No need to perform visibility test if the current edge is adjacent to the current vertex
            if (vert_idx == boundary_edges[2 * i]) {continue; }
            if (vert_idx == boundary_edges[2 * i + 1]) {continue; }

            double *vert = pointset->get_vertex(vert_idx); 
            double *edge_start = pointset->get_vertex(boundary_edges[2 * i]); 
            double *edge_end = pointset->get_vertex(boundary_edges[2 * i + 1]); 

            // Perform visibility test (see Caplan et al. 2020)
            if (orient2d(edge_start, edge_end, vert) < DET_THRESHOLD) {  // if fails
                visible_to_all_edges = false; 
                break; 
            }
        }

        // Add the valid receiving vertex if visibility test is passed 
        if (visible_to_all_edges) {
            receiving_vertices.push_back(vert_idx); 
        }
    }
}



// Choose the best receiving vertex in a digged polygon 
// Maximize the minimum mesh quality 
int choose_receiving_vertex(Topology *topology, vector<int>& boundary_edges, vector<int>& receiving_vertices, MetricField *metricfield) {
    double max_min = 0; 
    int choice = receiving_vertices[0]; 

    // For each candidate of receiving vertex
    for (int cand_v : receiving_vertices) {
        double curr_min = 1; 
        
        // Traverse potential new triangles 
        for (int i = 0; i < boundary_edges.size() / 2; i++) {
            int edge_start_idx = boundary_edges[2 * i]; 
            int edge_end_idx = boundary_edges[2 * i + 1]; 

            // Skip the edges that are adjacent to the receiving vertex
            if (cand_v == edge_start_idx) {continue; }
            if (cand_v == edge_end_idx) {continue; }

            // Compute quality of current triangle and update current minimum
            triangle_t tri = {cand_v, edge_start_idx, edge_end_idx}; 
            double qual = topology->get_quality(tri, metricfield); 
            curr_min = qual < curr_min ? qual : curr_min; 
        }

        // Update the choice of receiving vertex
        if (curr_min > max_min) {
            max_min = curr_min; 
            choice = cand_v; 
        }
    }

    return choice; 
}
