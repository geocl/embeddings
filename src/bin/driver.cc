#include "basics.h"
#include "utils.h"
#include "metric.h"
#include "scheduling.h"
#include "predicates/predicates.h"
#include <iostream>
#include <vector>
#include <array>
#include <map> 
#include <string>
#include <stdlib.h>
#include <time.h>
#include <Eigen/Core> 
#include <bits/stdc++.h> 
#include "testmetrics.h"

using namespace std; 
using namespace Eigen;

void report_results(Topology *topology, MetricField *metricfield); 


int main(int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        cout << "Usage: driver INPUT_FILE [OUTPUT_PREFIX]" << endl; 
        exit(1); 
    }

    // According to Shewchuck, exactinit() must be called at least once 
    exactinit(); 

    // [OUTPUT_PREFIX] is optional. If specified, get the file name prefix of where to store the adapted mesh 
    string datafile = argv[1], outfile_prefix; 
    if (argc >= 3) {
        outfile_prefix = argv[2]; 
    }
    
    // Iterate all tests
    for (int i = 0; i < testset_embeddings.size(); i++) {
        Vertices *pointset; 
        Topology *topology; 
        Embedding &embedding = testset_embeddings[i]; 
        MetricField *metricfield; 
        chrono::steady_clock::time_point begin; 
        chrono::steady_clock::time_point end; 
        double time_elapsed;

        // Number of iterations to run a full pass of mesh adapter 
        int total_iter = 15;
        // Whether to include mesh quality in exported file
        bool print_quality = true; 


        // ****** Part I: Compute by embedding, eval by metric ******

        pointset = new Vertices(0); 
        topology = new Topology(pointset); 
        // Read mesh
        read_topology(datafile, topology); 
        // Set random seed 
        srand(64); 
        // Construct metric field
        metricfield = new MetricField(
            CONSTRUCT_BY_EMBEDDING, 
            COMPUTE_BY_EMBEDDING, 
            embedding
        ); 
        // Perform mesh adaptation. Also measure time elapsed
        cout << "In the " << i << "th example, compute by embedding, eval by metric " << endl;
        begin = chrono::steady_clock::now();
        perform_schedule(topology, metricfield, total_iter, true); 
        end = chrono::steady_clock::now(); 
        time_elapsed = chrono::duration_cast<std::chrono::seconds>(end - begin).count();
        // Visualize
        // visualize_mesh(topology, false);
        cout << "Done." << endl; 
        // Export results to file 
        if (argc >= 3) {
            string outfile = outfile_prefix + "_" + to_string(i) + "_computeby_embedding_evalby_metric.txt"; 
            export_mesh_info(outfile , topology, print_quality, EVAL_BY_METRIC, metricfield, time_elapsed); 
        }
        // Cleanup
        delete pointset; 
        delete topology; 
        delete metricfield; 



        // ****** Part II: Compute by metric, eval by metric ******

        pointset = new Vertices(0); 
        topology = new Topology(pointset); 
        // Read mesh
        read_topology(datafile, topology); 
        // Set random seed 
        srand(64); 
        // Construct metric field
        metricfield = new MetricField(
            CONSTRUCT_BY_EMBEDDING, 
            COMPUTE_BY_METRIC, 
            embedding
        ); 
        // Perform mesh adaptation. Also measure time elapsed
        cout << "In the " << i << "th example, compute by embedding, eval by metric " << endl;
        begin = chrono::steady_clock::now();
        perform_schedule(topology, metricfield, total_iter, true); 
        end = chrono::steady_clock::now(); 
        time_elapsed = chrono::duration_cast<std::chrono::seconds>(end - begin).count();
        // Visualize
        // visualize_mesh(topology, false);
        cout << "Done." << endl; 
        // Export results to file 
        if (argc >= 3) {
            string outfile = outfile_prefix + "_" + to_string(i) + "_computeby_metric_evalby_metric.txt"; 
            export_mesh_info(outfile , topology, print_quality, EVAL_BY_METRIC, metricfield, time_elapsed); 
        }
        // Cleanup
        delete pointset; 
        delete topology; 
        delete metricfield;



        // ****** Part III: Compute by embedding, eval by embedding ******

        pointset = new Vertices(0); 
        topology = new Topology(pointset); 
        // Read mesh
        read_topology(datafile, topology); 
        // Set random seed 
        srand(64); 
        // Construct metric field
        metricfield = new MetricField(
            CONSTRUCT_BY_EMBEDDING, 
            COMPUTE_BY_EMBEDDING, 
            embedding
        ); 
        // Perform mesh adaptation. Also measure time elapsed
        cout << "In the " << i << "th example, compute by embedding, eval by metric " << endl;
        begin = chrono::steady_clock::now();
        perform_schedule(topology, metricfield, total_iter, true); 
        end = chrono::steady_clock::now(); 
        time_elapsed = chrono::duration_cast<std::chrono::seconds>(end - begin).count();
        // Visualize
        // visualize_mesh(topology, false);
        cout << "Done." << endl; 
        // Export results to file 
        if (argc >= 3) {
            string outfile = outfile_prefix + "_" + to_string(i) + "_computeby_embedding_evalby_embedding.txt"; 
            export_mesh_info(outfile , topology, print_quality, EVAL_BY_EMBEDDING, metricfield, time_elapsed); 
        }
        // Cleanup
        delete pointset; 
        delete topology; 
        delete metricfield; 



        // ****** Part IV: Compute by metric, eval by embedding ******

        pointset = new Vertices(0); 
        topology = new Topology(pointset); 
        // Read mesh
        read_topology(datafile, topology); 
        // Set random seed 
        srand(64); 
        // Construct metric field
        metricfield = new MetricField(
            CONSTRUCT_BY_EMBEDDING, 
            COMPUTE_BY_METRIC, 
            embedding
        ); 
        // Perform mesh adaptation. Also measure time elapsed
        cout << "In the " << i << "th example, compute by embedding, eval by metric " << endl;
        begin = chrono::steady_clock::now();
        perform_schedule(topology, metricfield, total_iter, true); 
        end = chrono::steady_clock::now(); 
        time_elapsed = chrono::duration_cast<std::chrono::seconds>(end - begin).count();
        // Visualize
        // visualize_mesh(topology, false);
        cout << "Done." << endl;
        // Export results to file 
        if (argc >= 3) {
            string outfile = outfile_prefix + "_" + to_string(i) + "_computeby_metric_evalby_embedding.txt"; 
            export_mesh_info(outfile , topology, print_quality, EVAL_BY_EMBEDDING, metricfield, time_elapsed); 
        }
        // Cleanup
        delete pointset; 
        delete topology; 
        delete metricfield;
        
    }
}



// void report_results(Topology *topology, MetricField *metricfield) {
//     for (auto e : topology->get_undir_edges_ordered(metricfield)) {
//             cout << "Edge " << e[0] << ", " << e[1] << " has length " << topology->get_length(e, metricfield) << endl; 
//         }
//         topology->start_iterator(); 
//         while(topology->has_triangle()) {
//             triangle_t tri = topology->next_triangle(); 
//             cout << "Triangle " << tri[0] << ", " << tri[1] << ", " << tri[2] << " has quality " << topology->get_quality(tri, metricfield) << endl; 
//         }
//         cout << endl; 
// }