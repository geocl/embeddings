// Self defined metric fields for testing

#include "metric.h"
#include <vector>
#include <Eigen/Core>

using namespace Eigen; 

#ifndef FIXED_DIR_ID_H
#define FIXED_DIR_ID_H

using namespace std; 

extern vector<MetricField> testset_metrics; 

extern vector<Embedding> testset_embeddings; 


#endif