#include "scheduling.h"
#include "basics.h"
#include "metric.h"
#include "scheduling.h"
#include "utils.h"
#include <stdlib.h>
#include <algorithm>
#include <fstream>

// Collapse all edges that are shorter than SHORT_EDGE_LEN
// Note: if we collapse an edge AB, whether vertex A or B will be removed is chosen randomly. The user may choose to specify a `seed` to produce replicable outcome for the random choice of "removed vertices"
void collapse_all_edges(Topology *topology, MetricField *metricfield) 
{
    vector<edge_t>& undir_edges_asc = topology->get_undir_edges_ordered(metricfield, true); 

    // Traverse all edges in ascending order of lengths 
    for (auto edge : undir_edges_asc) {
        // Skip long enough edges 
        if (topology->get_length(edge, metricfield) >= SHORT_EDGE_LEN) {
            return;    // Recall the edges are ordered by length. That's why I use `return`
        }

        // Current edge may have already been removed by other local operator
        // If so, proceed to next iteration 
        if (!topology->has_edge(edge)) {
            continue; 
        }

        // Randomly decide which vertex to try to remove first 
        int first_try = rand() % 2; 
        int next_try = 1 - first_try; 

        // Try remove vertex 
        unsigned short ret = topology->vertex_removal(edge[first_try], metricfield); 
        if (ret != 0) {
            topology->vertex_removal(edge[next_try], metricfield); 
        }
    }
}



// Split all edges that are longer than LONG_EDGE_LEN
void split_all_edges(Topology *topology, MetricField *metricfield) {
    // Traverse all edges in descending order of lengths
    for (auto edge : topology->get_undir_edges_ordered(metricfield, false)) {
        // Skip short enough edges 
        if (topology->get_length(edge, metricfield) <= LONG_EDGE_LEN) {
            // Debug
            return;    // Recall the edges are ordered by length. That's why I use `return`
        }

        // Current edge may have already been removed by other local operator
        // If so, proceed to next iteration 
        if (!topology->has_edge(edge)) {
            continue; 
        }

        // Split edge
        topology->edge_split(edge); 
    }
}



// Swap all edges that result in improvement of mesh qualities 
void swap_all_edges(Topology *topology, MetricField *metricfield) {
    for (auto edge : topology->get_undir_edges()) {
        // Current edge may have already been removed by other local operator
        // If so, proceed to next iteration 
        if (!topology->has_edge(edge)) {
            continue; 
        }

        bool has_tri_left = topology->has_edge(edge); 
        bool has_tri_right = topology->has_edge(rev(edge)); 

        // Skip if current edge is not shared by two triangles 
        if (!has_tri_left || !has_tri_right) {
            continue; 
        }

        triangle_t tri_left = topology->get_triangle(edge); 
        triangle_t tri_right = topology->get_triangle(rev(edge)); 
        double qual_left = topology->get_quality(tri_left, metricfield); 
        double qual_right = topology->get_quality(tri_right, metricfield); 
        double qual_min = min(qual_left, qual_right); 

        // Don't swap if current quality is good enough 
        if (qual_left >= TARGET_QUALITY && qual_right >= TARGET_QUALITY) {
            continue; 
        }

        int third_vert_left = topology->get_third_vertex(edge); 
        int third_vert_right = topology->get_third_vertex(rev(edge)); 

        // Swap if the minimum quality is improved 
        double new_qual_1 = topology->get_quality({third_vert_left, third_vert_right, edge[0]}, metricfield); 
        double new_qual_2 = topology->get_quality({third_vert_left, third_vert_right, edge[1]}, metricfield); 
        double new_qual_min = min(new_qual_1, new_qual_2); 

        if (new_qual_min > qual_min) {
            topology->edge_swap(edge); 
        }
    }
}



// Smooth all vertices
void smooth_all_vertices(Topology *topology, MetricField *metricfield) {
    Vertices *pointset = topology->pointset(); 
    int nb_vertices = pointset->nb(); 

    for (int v = 0; v < nb_vertices; v++) {
        // Skip vertices that are already removed 
        if (!pointset->exists(v)) {continue; }

        topology->vertex_smoothing(v, metricfield); 
    }
}



// The master function of adapting mesh 
// Perform the operators for nb_iter iterations. By default 10 iters 
void perform_schedule(Topology *topology, MetricField *metricfield, int nb_iter, bool smooth) {
    for (int i = 0; i < nb_iter; i++) {     
        collapse_all_edges(topology, metricfield); 
        split_all_edges(topology, metricfield);
        swap_all_edges(topology, metricfield); 
        if (smooth) {
            smooth_all_vertices(topology, metricfield);
        }
    }     
}

