#include "basics.h"
#include <string>
#include <iostream>

using namespace std; 

#ifndef UTILS_H
#define UTILS_H

// Modes for evaluating mesh quality in the final result 
#define EVAL_BY_METRIC 1
#define EVAL_BY_EMBEDDING -1 

void read_topology(string filename, Topology *topology);

// ================================
// Helpers 

edge_t rev(edge_t edge); 

void print_triangle(triangle_t triangle); 

void export_mesh_info(string filename, Topology *topology, bool print_quality = false, short eval_mode = EVAL_BY_METRIC, MetricField *metricfield = nullptr, double time_elapsed = -1); 

void visualize_mesh(Topology *topology, bool fork_process = true); 


#endif