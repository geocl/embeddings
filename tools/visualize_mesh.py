'''
visualize_mesh.py
An outdated version of visualizing mesh files 
'''

import turtle

if __name__ == '__main__':
    filepath = './bw_mesh_01.txt'
    scale = 270
    x_shift = -200
    y_shift = -260

    with open(filepath, 'r') as file: 
        # Number of vertices 
        nb_vertices = int(file.readline().rstrip())
        
        # Read coordinate of vertices and add them in a list
        vertices = []
        for i in range(nb_vertices): 
            x_str, y_str = file.readline().rstrip().split(' ')
            x, y = float(x_str), float(y_str)
            vertices.append((x, y))

        # Number of triangles
        nb_triangles = int(file.readline().rstrip())
        edges = set()

        # For each triangle, identify its edges by the tuple (vert1, vert2) where indices vert1 < vert2. Then add it to a set
        for i in range(nb_triangles): 
            v1_str, v2_str, v3_str = file.readline().rstrip().split(' ')
            v1, v2, v3 = int(v1_str), int(v2_str), int(v3_str)
            edges.add((min(v1, v2), max(v1, v2)))
            edges.add((min(v1, v3), max(v1, v3)))
            edges.add((min(v2, v3), max(v2, v3)))

        file.close()

    # Draw with turtle 
    scr = turtle.getscreen()
    cursor = turtle.Turtle()

    turtle.tracer(0, 0)    # Stop refreshing every time. 
    
    for edge in edges: 
        start_ind, end_ind = edge
        x_start, y_start = vertices[start_ind]
        x_end, y_end = vertices[end_ind]

        cursor.up()
        cursor.goto(x_start * scale + x_shift, y_start * scale + y_shift)
        cursor.down()
        cursor.goto(x_end * scale + x_shift, y_end * scale + y_shift)

    turtle.update()    # Update for the final graphic
    cursor.screen.mainloop()    # Pause the screen 



