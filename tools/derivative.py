from sympy import diff, tanh, cos, simplify, expand
from sympy.abc import x, y, pi
from sympy.matrices import Matrix, BlockMatrix
from sympy.printing.c import ccode

if __name__ == '__main__': 
    f_coefs = [0.5, 1, 3, 5]

    for f in f_coefs: 
        print(f'f = {f}')

        # The lift function 
        lift = 5 * tanh( f * (y - cos(pi * x / 5) - 5) )

        # The embedding expression 
        embed = Matrix([x, y, lift])

        # Differentiate
        ddx = diff(embed, x)
        ddy = diff(embed, y)

        # Get Jacobian (i.e. gradient)
        grad = ddx.row_join(ddy)
        
        # Get induced metric 
        induced_metric = grad.transpose() * grad

        print('Entry 0, 0 of induced metric: ', ccode(induced_metric[0, 0]))
        print('Entry 0, 1 of induced metric: ', ccode(induced_metric[0, 1]))
        print('Entry 1, 0 of induced metric: ', ccode(induced_metric[1, 0]))
        print('Entry 1, 1 of induced metric: ', ccode(induced_metric[1, 1]))
        print('')
